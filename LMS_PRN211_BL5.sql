USE [master]
GO
/****** Object:  Database [LMS_PRN211]    Script Date: 11/25/2023 1:55:17 AM ******/
CREATE DATABASE [LMS_PRN211]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LMS_PRN211', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\LMS_PRN211.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LMS_PRN211_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\LMS_PRN211_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [LMS_PRN211] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LMS_PRN211].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LMS_PRN211] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LMS_PRN211] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LMS_PRN211] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LMS_PRN211] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LMS_PRN211] SET ARITHABORT OFF 
GO
ALTER DATABASE [LMS_PRN211] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LMS_PRN211] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LMS_PRN211] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LMS_PRN211] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LMS_PRN211] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LMS_PRN211] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LMS_PRN211] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LMS_PRN211] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LMS_PRN211] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LMS_PRN211] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LMS_PRN211] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LMS_PRN211] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LMS_PRN211] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LMS_PRN211] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LMS_PRN211] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LMS_PRN211] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LMS_PRN211] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LMS_PRN211] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LMS_PRN211] SET  MULTI_USER 
GO
ALTER DATABASE [LMS_PRN211] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LMS_PRN211] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LMS_PRN211] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LMS_PRN211] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LMS_PRN211] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LMS_PRN211] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [LMS_PRN211] SET QUERY_STORE = ON
GO
ALTER DATABASE [LMS_PRN211] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [LMS_PRN211]
GO
/****** Object:  Table [dbo].[Author]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Author](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Author] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[id] [int] NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[ISBN] [varchar](30) NOT NULL,
	[edition] [int] NOT NULL,
	[publisher] [varchar](50) NOT NULL,
	[description] [nvarchar](200) NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book_Author]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book_Author](
	[book_id] [int] NOT NULL,
	[author_id] [int] NOT NULL,
 CONSTRAINT [PK_Book_Author] PRIMARY KEY CLUSTERED 
(
	[book_id] ASC,
	[author_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book_Detail]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book_Detail](
	[book_id] [int] NOT NULL,
	[book_location] [varchar](10) NOT NULL,
	[num_of_copies] [int] NOT NULL,
	[format] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Book_Detail] PRIMARY KEY CLUSTERED 
(
	[book_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book_Genre]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book_Genre](
	[book_id] [int] NOT NULL,
	[genre_id] [int] NOT NULL,
 CONSTRAINT [PK_book_genre] PRIMARY KEY CLUSTERED 
(
	[book_id] ASC,
	[genre_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[book_image]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[book_image](
	[id] [int] NOT NULL,
	[img_url] [nvarchar](max) NULL,
 CONSTRAINT [PK_image_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Borrow_Details]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Borrow_Details](
	[id] [int] NOT NULL,
	[bookID] [int] NOT NULL,
	[quantity] [int] NOT NULL,
 CONSTRAINT [pk_borrowId_bookId] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[bookID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Borrow_Record]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Borrow_Record](
	[id] [int] NOT NULL,
	[borrower_id] [int] NOT NULL,
	[librarian_id] [int] NOT NULL,
	[start_date] [datetime] NOT NULL,
	[end_date] [datetime] NOT NULL,
	[return_date] [datetime] NULL,
	[status] [varchar](10) NOT NULL,
	[fine] [real] NULL,
 CONSTRAINT [PK_Record_Book] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Borrower]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Borrower](
	[id] [int] NOT NULL,
	[name] [varchar](50) NULL,
	[gender] [bit] NULL,
	[phone] [varchar](15) NULL,
	[address] [varchar](255) NULL,
 CONSTRAINT [PK_Borrower] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Genres]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genres](
	[id] [int] NOT NULL,
	[genre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_genre] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Librarian]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Librarian](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Librarian] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Token]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Token](
	[Id] [int] NOT NULL,
	[Token] [varchar](25) NOT NULL,
	[UserId] [int] NOT NULL,
	[GeneratedTime] [datetime] NOT NULL,
	[Status] [varchar](5) NOT NULL,
 CONSTRAINT [pk_id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User_role]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_role](
	[id] [int] NOT NULL,
	[role_name] [varchar](10) NOT NULL,
 CONSTRAINT [PK_User_role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 11/25/2023 1:55:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] NOT NULL,
	[email] [varchar](100) NOT NULL,
	[hashed_password] [varbinary](32) NULL,
	[salt] [varbinary](16) NULL,
	[role] [int] NULL,
	[status] [nvarchar](20) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Author] ([id], [name]) VALUES (1, N'J.R.Tolkiens')
GO
INSERT [dbo].[Author] ([id], [name]) VALUES (2, N'J.K.Rowlings')
GO
INSERT [dbo].[Author] ([id], [name]) VALUES (3, N'Donald Trump')
GO
INSERT [dbo].[Author] ([id], [name]) VALUES (4, N'Robert Kiyosaki')
GO
INSERT [dbo].[Book] ([id], [title], [ISBN], [edition], [publisher], [description]) VALUES (1, N'The Lord Of The Rings', N'978-0544003415', 1, N'William Morrow Paperbacks', N'Chúa Tể Những Chiếc Nhẫn (TLOTR) là cuốn tiểu thuyết kinh điển, được chuyển thể thành bộ phim nổi tiếng')
GO
INSERT [dbo].[Book] ([id], [title], [ISBN], [edition], [publisher], [description]) VALUES (2, N'The Hobbit', N'978-0547928227', 1, N'Publisher Houghton Mifflin Harcourt', N'Tiếp tục là một cuốn tiểu thuyết nổi tiếng của TLOTR, cuốn tiểu thuyết kể về tộc người lùn Hobbit và câu chuyện phiêu lưu của họ')
GO
INSERT [dbo].[Book] ([id], [title], [ISBN], [edition], [publisher], [description]) VALUES (3, N'Inferno - Hỏa Ngục', N'978-1400079155', 1, N'Anchor', N'Hỏa Ngục là cuốn tiểu thuyết kể về cuộc hành trình của nhân vật ...')
GO
INSERT [dbo].[Book] ([id], [title], [ISBN], [edition], [publisher], [description]) VALUES (4, N'The Two Towers', N'978-0547928203', 1, N'William Morrow Paperbacks', N'Hai Tòa Tháp là phần truyệt thứ hai trong bộ tiểu thuyết kinh điển TLOTR của nhà văn J.R.R Tolkien')
GO
INSERT [dbo].[Book_Author] ([book_id], [author_id]) VALUES (1, 1)
GO
INSERT [dbo].[Book_Author] ([book_id], [author_id]) VALUES (2, 1)
GO
INSERT [dbo].[Book_Author] ([book_id], [author_id]) VALUES (3, 3)
GO
INSERT [dbo].[Book_Author] ([book_id], [author_id]) VALUES (4, 1)
GO
INSERT [dbo].[Book_Detail] ([book_id], [book_location], [num_of_copies], [format]) VALUES (1, N'GIA-SACH-1', 13, N'PDF')
GO
INSERT [dbo].[Book_Detail] ([book_id], [book_location], [num_of_copies], [format]) VALUES (2, N'GIA-SACH-2', 101, N'EPUB')
GO
INSERT [dbo].[Book_Detail] ([book_id], [book_location], [num_of_copies], [format]) VALUES (3, N'GIA-SACH-1', 10, N'HARD COPY')
GO
INSERT [dbo].[Book_Detail] ([book_id], [book_location], [num_of_copies], [format]) VALUES (4, N'GIA-SACH-3', 12, N'PDF')
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (1, 1)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (1, 2)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (1, 3)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (2, 4)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (2, 6)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (2, 9)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (3, 3)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (3, 4)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (4, 5)
GO
INSERT [dbo].[Book_Genre] ([book_id], [genre_id]) VALUES (4, 6)
GO
INSERT [dbo].[Borrow_Details] ([id], [bookID], [quantity]) VALUES (1, 1, 3)
GO
INSERT [dbo].[Borrow_Details] ([id], [bookID], [quantity]) VALUES (1, 2, 1)
GO
INSERT [dbo].[Borrow_Record] ([id], [borrower_id], [librarian_id], [start_date], [end_date], [return_date], [status], [fine]) VALUES (1, 2, 1, CAST(N'2023-11-11T00:00:00.000' AS DateTime), CAST(N'2023-11-11T00:00:00.000' AS DateTime), CAST(N'2023-11-24T18:49:36.050' AS DateTime), N'RETURNED', 0)
GO
INSERT [dbo].[Borrower] ([id], [name], [gender], [phone], [address]) VALUES (2, N'Thang san', 1, N'0382721058', N'Thai Nguyen')
GO
INSERT [dbo].[Borrower] ([id], [name], [gender], [phone], [address]) VALUES (3, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Borrower] ([id], [name], [gender], [phone], [address]) VALUES (4, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (1, N'Horror')
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (2, N'Adventure')
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (3, N'Fantasy')
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (4, N'History')
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (5, N'Self-help')
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (6, N'Economy')
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (7, N'Politics')
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (8, N'Comedy')
GO
INSERT [dbo].[Genres] ([id], [genre]) VALUES (9, N'Violence')
GO
INSERT [dbo].[Librarian] ([id], [name]) VALUES (1, N'Ha Quang Thang')
GO
INSERT [dbo].[Token] ([Id], [Token], [UserId], [GeneratedTime], [Status]) VALUES (1, N'6dUiw6iCEUaO0Y38cgmiTg==', 2, CAST(N'2023-11-11T11:29:11.143' AS DateTime), N'U')
GO
INSERT [dbo].[Token] ([Id], [Token], [UserId], [GeneratedTime], [Status]) VALUES (2, N'o4LW+XZnhUyyI5teO2mtqA==', 3, CAST(N'2023-11-24T21:42:25.653' AS DateTime), N'U')
GO
INSERT [dbo].[Token] ([Id], [Token], [UserId], [GeneratedTime], [Status]) VALUES (3, N'z0p9jAc1f02W5sH6wWm00g==', 4, CAST(N'2023-11-24T21:51:42.940' AS DateTime), N'U')
GO
INSERT [dbo].[User_role] ([id], [role_name]) VALUES (1, N'Librarian')
GO
INSERT [dbo].[User_role] ([id], [role_name]) VALUES (2, N'Borrower')
GO
INSERT [dbo].[User_role] ([id], [role_name]) VALUES (3, N'Author')
GO
INSERT [dbo].[Users] ([id], [email], [hashed_password], [salt], [role], [status]) VALUES (1, N'librarian@gmail.com', 0x29A7DD3D09442A6A1CADF5D5646CC8F018FFECDB055F945DB6365C941DB091E1, 0xA38B265642BA5C160D165177BFFB282F, 1, N'ACTIVE')
GO
INSERT [dbo].[Users] ([id], [email], [hashed_password], [salt], [role], [status]) VALUES (2, N'a@gmail.com', 0xB0F3757259AEFC8EC7D471EDC86EA3659F162BE01703FFC8C0FEC3CE19DC46CD, 0xEB12BD09AA0BFC48A4A79E4D1548DF22, 2, N'UNACTIVATE')
GO
INSERT [dbo].[Users] ([id], [email], [hashed_password], [salt], [role], [status]) VALUES (3, N'tiensy4th@gmail.com', 0x606ADF7E387144AEA2A99B6365F41CBDEEC36E6C2F36F4998E767A4152EB7C72, 0x92F123A374D06DEABBA998C14D4D9AAE, 2, N'UNACTIVATE')
GO
INSERT [dbo].[Users] ([id], [email], [hashed_password], [salt], [role], [status]) VALUES (4, N'sss@gmail.com', 0xE535235D6BDC0017E5104A4F8787CD4BA448895F192ED89B4DE8F6B6182FF01D, 0xBF1292828FB6972A13693C57B37749FC, 2, N'UNACTIVATE')
GO
ALTER TABLE [dbo].[Book_Author]  WITH CHECK ADD  CONSTRAINT [FK_Author] FOREIGN KEY([author_id])
REFERENCES [dbo].[Author] ([id])
GO
ALTER TABLE [dbo].[Book_Author] CHECK CONSTRAINT [FK_Author]
GO
ALTER TABLE [dbo].[Book_Author]  WITH CHECK ADD  CONSTRAINT [FK_Book] FOREIGN KEY([book_id])
REFERENCES [dbo].[Book] ([id])
GO
ALTER TABLE [dbo].[Book_Author] CHECK CONSTRAINT [FK_Book]
GO
ALTER TABLE [dbo].[Book_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Book_Detail] FOREIGN KEY([book_id])
REFERENCES [dbo].[Book] ([id])
GO
ALTER TABLE [dbo].[Book_Detail] CHECK CONSTRAINT [FK_Book_Detail]
GO
ALTER TABLE [dbo].[Book_Genre]  WITH CHECK ADD  CONSTRAINT [FK_book_genre] FOREIGN KEY([genre_id])
REFERENCES [dbo].[Genres] ([id])
GO
ALTER TABLE [dbo].[Book_Genre] CHECK CONSTRAINT [FK_book_genre]
GO
ALTER TABLE [dbo].[Book_Genre]  WITH CHECK ADD  CONSTRAINT [FK_bookId_genre] FOREIGN KEY([book_id])
REFERENCES [dbo].[Book] ([id])
GO
ALTER TABLE [dbo].[Book_Genre] CHECK CONSTRAINT [FK_bookId_genre]
GO
ALTER TABLE [dbo].[book_image]  WITH CHECK ADD  CONSTRAINT [FK_image_id] FOREIGN KEY([id])
REFERENCES [dbo].[Book] ([id])
GO
ALTER TABLE [dbo].[book_image] CHECK CONSTRAINT [FK_image_id]
GO
ALTER TABLE [dbo].[Borrow_Details]  WITH CHECK ADD  CONSTRAINT [fk_bookId] FOREIGN KEY([bookID])
REFERENCES [dbo].[Book] ([id])
GO
ALTER TABLE [dbo].[Borrow_Details] CHECK CONSTRAINT [fk_bookId]
GO
ALTER TABLE [dbo].[Borrow_Details]  WITH CHECK ADD  CONSTRAINT [fk_Borrow] FOREIGN KEY([id])
REFERENCES [dbo].[Borrow_Record] ([id])
GO
ALTER TABLE [dbo].[Borrow_Details] CHECK CONSTRAINT [fk_Borrow]
GO
ALTER TABLE [dbo].[Borrow_Record]  WITH CHECK ADD  CONSTRAINT [FK_Record_Borrower] FOREIGN KEY([borrower_id])
REFERENCES [dbo].[Borrower] ([id])
GO
ALTER TABLE [dbo].[Borrow_Record] CHECK CONSTRAINT [FK_Record_Borrower]
GO
ALTER TABLE [dbo].[Borrow_Record]  WITH CHECK ADD  CONSTRAINT [FK_Record_Librarian] FOREIGN KEY([librarian_id])
REFERENCES [dbo].[Librarian] ([id])
GO
ALTER TABLE [dbo].[Borrow_Record] CHECK CONSTRAINT [FK_Record_Librarian]
GO
ALTER TABLE [dbo].[Borrower]  WITH CHECK ADD  CONSTRAINT [FK_Borrower_User] FOREIGN KEY([id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Borrower] CHECK CONSTRAINT [FK_Borrower_User]
GO
ALTER TABLE [dbo].[Librarian]  WITH CHECK ADD  CONSTRAINT [FK_Librarian_User] FOREIGN KEY([id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Librarian] CHECK CONSTRAINT [FK_Librarian_User]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [fk_userId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [fk_userId]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Role] FOREIGN KEY([role])
REFERENCES [dbo].[User_role] ([id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Role]
GO
USE [master]
GO
ALTER DATABASE [LMS_PRN211] SET  READ_WRITE 
GO
