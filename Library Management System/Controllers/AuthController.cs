﻿using Microsoft.AspNetCore.Mvc;
using Library_Management_System.Services;
using Library_Management_System.ViewModels;
using Library_Management_System.Models;
using System.Text.Json;

namespace Library_Management_System.Controllers
{
    public class AuthController : Controller
    {
        private readonly ILogin _loginService;
        private readonly IHttpContextAccessor _contextAccessor;

        public AuthController(ILogin login, IHttpContextAccessor httpContextAccessor)
        {
            _loginService = login;
            _contextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Display the login form. Logged user cannot navigate to this page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            ISession session = HttpContext.Session;
            if (session.GetString("user") == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// Handle login logic
        /// </summary>
        /// <param name="loginView"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DoLogin(LoginViewModel loginView)
        {
            User? loggedUser = _loginService.Login(loginView.Username, loginView.Password);
            if (loggedUser != null) // login sucess
            {
                // TODO: set user object to session
                ISession session = _contextAccessor.HttpContext.Session;
                session.SetString("user", JsonSerializer.Serialize(loggedUser));
                if(loggedUser.Role == 1)
                {
                    return RedirectToAction("Index", "Admin");
                }

                if(loggedUser.Role == 2)
                {
                    return RedirectToAction("Index", "Home");
                }

                return NotFound();
            }
            else // login failed
            {
                ViewBag.Message = "Wrong username or password";
                return View("Index");
            }
        }

        [HttpGet]
        public IActionResult Logout()
        {
            ISession session = _contextAccessor.HttpContext.Session;
            session.Clear();
            return RedirectToAction("Index", "Home");
        }

        //[HttpGet]
        //public IActionResult RegisterIndex()
        //{

        //}

        //[HttpPost]
        //public IActionResult DoRegister()
        //{

        //}

    }
}
