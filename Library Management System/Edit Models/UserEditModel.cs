﻿using Library_Management_System.Models;
using System.ComponentModel.DataAnnotations;

namespace Library_Management_System.Edit_Models
{
    /// <summary>
    /// This model class is used in Create and Edit form for user
    /// </summary>
    public class UserEditModel
    {
        // User information
        public string Email { get; set; }
        public string Password { get; set; }
        [Display(Name = "Retype your password", Description = "Retype your password")]
        public string RetypePwd { get; set; }
        [Display(Name = "Role name")]
        public int? RoleId { get; set; }

        // Role list for Librarian to choose from
        [Display(Name = "Role list")]
        public IEnumerable<UserRole> RoleList { get; set; }
    }
}
