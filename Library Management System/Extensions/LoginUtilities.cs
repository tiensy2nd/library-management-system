﻿using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Library_Management_System.Extensions
{
    public class LoginUtilities
    {
        /// <summary>
        /// Generate a 128-bit salt using a sequence of cryptographically strong random bytes. 
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateSalt()
        {
            // Generate a 128-bit salt using a sequence of cryptographically strong random bytes.
            byte[] salt = RandomNumberGenerator.GetBytes(128 / 8); // divide by 8 to convert bits to bytes
            return salt;
        }

        /// <summary>
        /// Derive a 256-bit subkey (use HMACSHA256 with 100,000 iterations)
        /// </summary>
        /// <param name="rawPassword">Raw password user used to login</param>
        /// <param name="salt">salt used to hash the password</param>
        /// <returns>string of hashed password</returns>
        public static byte[] HashPassword(string rawPassword, byte[] salt)
        {
            byte[] hashed = KeyDerivation.Pbkdf2(
                password: rawPassword,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256, // Using Pseudo Random Function (PRF) SHA256
                iterationCount: 100000, // key stretching: hashing password 100000 times
                numBytesRequested: 256 / 8); // readability: 256-bit key --> 32-bytes key

            return hashed;
        }

        /// <summary>
        /// Generate a unique token from GUID
        /// </summary>
        /// <returns>A sequence of length 24 characters</returns>
        public static string GenerateToken()
        {
            string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            return token;
        }

    }
}
