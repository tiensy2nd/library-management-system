﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class Book
    {
        public Book()
        {
            BorrowDetails = new HashSet<BorrowDetail>();
            Authors = new HashSet<Author>();
            Genres = new HashSet<Genre>();
        }

        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string Isbn { get; set; } = null!;
        public int Edition { get; set; }
        public string Publisher { get; set; } = null!;
        public string? Description { get; set; }

        public virtual BookDetail? BookDetail { get; set; }
        public virtual BookImage? BookImage { get; set; }
        public virtual ICollection<BorrowDetail> BorrowDetails { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
        public virtual ICollection<Genre> Genres { get; set; }
    }
}
