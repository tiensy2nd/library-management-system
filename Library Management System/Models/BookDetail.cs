﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class BookDetail
    {
        public int BookId { get; set; }
        public string BookLocation { get; set; } = null!;
        public int NumOfCopies { get; set; }
        public string Format { get; set; } = null!;

        public virtual Book Book { get; set; } = null!;
    }
}
