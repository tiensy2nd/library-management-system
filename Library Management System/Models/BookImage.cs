﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class BookImage
    {
        public int Id { get; set; }
        public string? ImgUrl { get; set; }

        public virtual Book IdNavigation { get; set; } = null!;
    }
}
