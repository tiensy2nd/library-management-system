﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class BorrowDetail
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int Quantity { get; set; }

        public virtual Book Book { get; set; } = null!;
        public virtual BorrowRecord IdNavigation { get; set; } = null!;
    }
}
