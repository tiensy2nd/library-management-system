﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class BorrowRecord
    {
        public BorrowRecord()
        {
            BorrowDetails = new HashSet<BorrowDetail>();
        }

        public int Id { get; set; }
        public int BorrowerId { get; set; }
        public int LibrarianId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public string Status { get; set; } = null!;
        public float? Fine { get; set; }

        public virtual Borrower Borrower { get; set; } = null!;
        public virtual Librarian Librarian { get; set; } = null!;
        public virtual ICollection<BorrowDetail> BorrowDetails { get; set; }
    }
}
