﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class Borrower
    {
        public Borrower()
        {
            BorrowRecords = new HashSet<BorrowRecord>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public bool? Gender { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }

        public virtual User IdNavigation { get; set; } = null!;
        public virtual ICollection<BorrowRecord> BorrowRecords { get; set; }
    }
}
