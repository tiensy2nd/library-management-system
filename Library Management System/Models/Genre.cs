﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class Genre
    {
        public Genre()
        {
            Books = new HashSet<Book>();
        }

        public int Id { get; set; }
        public string Genre1 { get; set; } = null!;

        public virtual ICollection<Book> Books { get; set; }
    }
}
