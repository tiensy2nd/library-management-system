﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Library_Management_System.ViewModels;

namespace Library_Management_System.Models
{
    public partial class LMS_PRN211Context : DbContext
    {
        public LMS_PRN211Context()
        {
        }

        public LMS_PRN211Context(DbContextOptions<LMS_PRN211Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Author> Authors { get; set; } = null!;
        public virtual DbSet<Book> Books { get; set; } = null!;
        public virtual DbSet<BookDetail> BookDetails { get; set; } = null!;
        public virtual DbSet<BookImage> BookImages { get; set; } = null!;
        public virtual DbSet<BorrowDetail> BorrowDetails { get; set; } = null!;
        public virtual DbSet<BorrowRecord> BorrowRecords { get; set; } = null!;
        public virtual DbSet<Borrower> Borrowers { get; set; } = null!;
        public virtual DbSet<Genre> Genres { get; set; } = null!;
        public virtual DbSet<Librarian> Librarians { get; set; } = null!;
        public virtual DbSet<Token> Tokens { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<UserRole> UserRoles { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server =(local); database = LMS_PRN211;uid=sa;pwd=123456;TrustServerCertificate=True;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>(entity =>
            {
                entity.ToTable("Author");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Book>(entity =>
            {
                entity.ToTable("Book");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .HasColumnName("description");

                entity.Property(e => e.Edition).HasColumnName("edition");

                entity.Property(e => e.Isbn)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("ISBN");

                entity.Property(e => e.Publisher)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("publisher");

                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .HasColumnName("title");

                entity.HasMany(d => d.Authors)
                    .WithMany(p => p.Books)
                    .UsingEntity<Dictionary<string, object>>(
                        "BookAuthor",
                        l => l.HasOne<Author>().WithMany().HasForeignKey("AuthorId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_Author"),
                        r => r.HasOne<Book>().WithMany().HasForeignKey("BookId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_Book"),
                        j =>
                        {
                            j.HasKey("BookId", "AuthorId");

                            j.ToTable("Book_Author");

                            j.IndexerProperty<int>("BookId").HasColumnName("book_id");

                            j.IndexerProperty<int>("AuthorId").HasColumnName("author_id");
                        });

                entity.HasMany(d => d.Genres)
                    .WithMany(p => p.Books)
                    .UsingEntity<Dictionary<string, object>>(
                        "BookGenre",
                        l => l.HasOne<Genre>().WithMany().HasForeignKey("GenreId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_book_genre"),
                        r => r.HasOne<Book>().WithMany().HasForeignKey("BookId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_bookId_genre"),
                        j =>
                        {
                            j.HasKey("BookId", "GenreId").HasName("PK_book_genre");

                            j.ToTable("Book_Genre");

                            j.IndexerProperty<int>("BookId").HasColumnName("book_id");

                            j.IndexerProperty<int>("GenreId").HasColumnName("genre_id");
                        });
            });

            modelBuilder.Entity<BookDetail>(entity =>
            {
                entity.HasKey(e => e.BookId);

                entity.ToTable("Book_Detail");

                entity.Property(e => e.BookId)
                    .ValueGeneratedNever()
                    .HasColumnName("book_id");

                entity.Property(e => e.BookLocation)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("book_location");

                entity.Property(e => e.Format)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("format");

                entity.Property(e => e.NumOfCopies).HasColumnName("num_of_copies");

                entity.HasOne(d => d.Book)
                    .WithOne(p => p.BookDetail)
                    .HasForeignKey<BookDetail>(d => d.BookId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Book_Detail");
            });

            modelBuilder.Entity<BookImage>(entity =>
            {
                entity.ToTable("book_image");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.ImgUrl).HasColumnName("img_url");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.BookImage)
                    .HasForeignKey<BookImage>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_image_id");
            });

            modelBuilder.Entity<BorrowDetail>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.BookId })
                    .HasName("pk_borrowId_bookId");

                entity.ToTable("Borrow_Details");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BookId).HasColumnName("bookID");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.BorrowDetails)
                    .HasForeignKey(d => d.BookId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_bookId");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.BorrowDetails)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Borrow");
            });

            modelBuilder.Entity<BorrowRecord>(entity =>
            {
                entity.ToTable("Borrow_Record");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.BorrowerId).HasColumnName("borrower_id");

                entity.Property(e => e.EndDate)
                    .HasColumnType("datetime")
                    .HasColumnName("end_date");

                entity.Property(e => e.Fine).HasColumnName("fine");

                entity.Property(e => e.LibrarianId).HasColumnName("librarian_id");

                entity.Property(e => e.ReturnDate)
                    .HasColumnType("datetime")
                    .HasColumnName("return_date");

                entity.Property(e => e.StartDate)
                    .HasColumnType("datetime")
                    .HasColumnName("start_date");

                entity.Property(e => e.Status)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("status");

                entity.HasOne(d => d.Borrower)
                    .WithMany(p => p.BorrowRecords)
                    .HasForeignKey(d => d.BorrowerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Record_Borrower");

                entity.HasOne(d => d.Librarian)
                    .WithMany(p => p.BorrowRecords)
                    .HasForeignKey(d => d.LibrarianId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Record_Librarian");
            });

            modelBuilder.Entity<Borrower>(entity =>
            {
                entity.ToTable("Borrower");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("address");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.Property(e => e.Phone)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("phone");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Borrower)
                    .HasForeignKey<Borrower>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Borrower_User");
            });

            modelBuilder.Entity<Genre>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Genre1)
                    .HasMaxLength(50)
                    .HasColumnName("genre");
            });

            modelBuilder.Entity<Librarian>(entity =>
            {
                entity.ToTable("Librarian");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Librarian)
                    .HasForeignKey<Librarian>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Librarian_User");
            });

            modelBuilder.Entity<Token>(entity =>
            {
                entity.ToTable("Token");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.GeneratedTime).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Token1)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("Token");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Tokens)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_userId");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.HashedPassword)
                    .HasMaxLength(32)
                    .HasColumnName("hashed_password");

                entity.Property(e => e.Role).HasColumnName("role");

                entity.Property(e => e.Salt)
                    .HasMaxLength(16)
                    .HasColumnName("salt");

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .HasColumnName("status");

                entity.HasOne(d => d.RoleNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.Role)
                    .HasConstraintName("FK_Users_Role");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.ToTable("User_role");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.RoleName)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("role_name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public DbSet<Library_Management_System.ViewModels.BookAddModel>? BookAddModel { get; set; }
    }
}
