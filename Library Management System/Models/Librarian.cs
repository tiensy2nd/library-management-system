﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class Librarian
    {
        public Librarian()
        {
            BorrowRecords = new HashSet<BorrowRecord>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;

        public virtual User IdNavigation { get; set; } = null!;
        public virtual ICollection<BorrowRecord> BorrowRecords { get; set; }
    }
}
