﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class Token
    {
        public int Id { get; set; }
        public string Token1 { get; set; } = null!;
        public int UserId { get; set; }
        public DateTime GeneratedTime { get; set; }
        public string Status { get; set; } = null!;

        public virtual User User { get; set; } = null!;
    }
}
