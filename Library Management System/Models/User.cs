﻿using System;
using System.Collections.Generic;

namespace Library_Management_System.Models
{
    public partial class User
    {
        public User()
        {
            Tokens = new HashSet<Token>();
        }

        public int Id { get; set; }
        public string Email { get; set; } = null!;
        public byte[]? HashedPassword { get; set; }
        public byte[]? Salt { get; set; }
        public int? Role { get; set; }
        public string? Status { get; set; }

        public virtual UserRole? RoleNavigation { get; set; }
        public virtual Borrower? Borrower { get; set; }
        public virtual Librarian? Librarian { get; set; }
        public virtual ICollection<Token> Tokens { get; set; }
    }
}
