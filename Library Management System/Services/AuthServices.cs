﻿using Library_Management_System.Models;
using Library_Management_System.Extensions;

namespace Library_Management_System.Services
{
    public class AuthServices : ILogin
    {
        private readonly LMS_PRN211Context _context;
        private readonly IUserServices _userServices;

        public AuthServices(LMS_PRN211Context context, IUserServices userServices)
        {
            _context = context;
            _userServices = userServices;
        }

        public bool IsLoggedIn => throw new NotImplementedException();

        public User? Login(string email, string password)
        {
            User? user = _userServices.GetUserByEmail(email);

            // there is no account with this email
            if (user == null)
            {
                return null;
            }

            byte[] salt = user.Salt;
            byte[] realHashed = user.HashedPassword; // real hashed password in db
            // hashing user inputed password
            byte[] enteredHash = LoginUtilities.HashPassword(password, salt);
            // compare hashed password
            if (realHashed.SequenceEqual(enteredHash)) // login success, correct password
            {
                return user;
            }
            // login failed, password wrong
            return null;
        }
    }
}
