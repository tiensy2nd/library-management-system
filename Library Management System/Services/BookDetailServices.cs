﻿using Library_Management_System.Models;

namespace Library_Management_System.Services
{
    public class BookDetailServices : IBookDetailServices
    {   
        private readonly LMS_PRN211Context _context;
        public BookDetailServices(LMS_PRN211Context context) 
        {
            _context = context;
        }

        public void AddBookDetail(BookDetail bookDetail)
        {
            bookDetail.BookId = GetBookDetailCount() + 1;
            _context.BookDetails.Add(bookDetail);
            _context.SaveChanges();
        }

        public BookDetail? GetBookDetailById(int id)
        {
            BookDetail? bookDetail = _context.BookDetails.SingleOrDefault(x => x.BookId == id);
            return bookDetail;
        }

        public int GetBookDetailCount()
        {
            return _context.BookDetails.Count();
        }

        public IEnumerable<BookDetail> GetBookDetails()
        {
            IEnumerable<BookDetail> bookDetails = _context.BookDetails;
            return bookDetails;
        }
    }
}
