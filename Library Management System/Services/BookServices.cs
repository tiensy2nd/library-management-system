﻿using Library_Management_System.Models;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Library_Management_System.Services
{
    public class BookServices : IBookServices
    {
        private readonly LMS_PRN211Context _context;
        public BookServices(LMS_PRN211Context context) { 
            _context = context;
        }

        public void AddBook(Book book)
        {
            book.Id = GetBookCount()+1;
            _context.Books.Add(book);
            _context.SaveChanges();
        }

        public void AddBookImage(string url)
        {
            throw new NotImplementedException();
        }

        public Book? GetBookById(int id)
        {
            Book? book=_context.Books.SingleOrDefault(x => x.Id == id);
            return book;
        }

        public Book? GetBookByTitle(string title)
        {
            Book? book=_context.Books.SingleOrDefault(x => x.Title == title);
            return book;
        }

        public int GetBookCount()
        {
            int count = _context.Books.Count();
            return count;
        }

        public IEnumerable<Book> GetBooks()
        {
            IEnumerable<Book> books= _context.Books;
            return books;
        }
    }
}
