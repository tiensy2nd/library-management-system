﻿using Library_Management_System.Models;

namespace Library_Management_System.Services
{
    public interface IBookDetailServices
    {
        IEnumerable<BookDetail> GetBookDetails();
        BookDetail? GetBookDetailById(int id);
        void AddBookDetail(BookDetail bookDetail);
        int GetBookDetailCount();
        
    }
}
