﻿using Library_Management_System.Models;
namespace Library_Management_System.Services
{
    public interface IBookServices
    {
        IEnumerable<Book> GetBooks();
        Book? GetBookById(int id);
        Book? GetBookByTitle(string title);
        int GetBookCount();
        void AddBookImage(string url);
        void AddBook(Book book);
    }
}
