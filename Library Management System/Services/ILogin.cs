﻿using Library_Management_System.Models;

namespace Library_Management_System.Services
{
    public interface ILogin
    {
        bool IsLoggedIn { get; }

        /// <summary>
        /// Perform login function for web application
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns>null if login fail. User object corresponding to logged user if login success</returns>
        User? Login(string email, string password);
    }
}
