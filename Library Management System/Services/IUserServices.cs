﻿using Library_Management_System.Models;
using Library_Management_System.Edit_Models;

namespace Library_Management_System.Services
{
    public interface IUserServices
    {
        IEnumerable<User> GetUsers();
        IEnumerable<Borrower> GetBorrowers();
        User? GetUserById(int id);
        User? GetUserByEmail(string email);
        User? AddUser(User user);
        IEnumerable<UserRole> GetRoles();
        User? CreateUser(UserEditModel userEditModel);
        int GetLastId();
        Librarian? GetLibrarianById(int librarianId);
        /// <summary>
        /// Add token corresponding to user in Token table
        /// </summary>
        /// <param name="token"></param>
        /// <param name="userId"></param>
        void AddToken(string token, int userId);
        int GetLastTokenId();
        User? GetUserByToken(string token);
        void UpdateUser(User user);
    }
}
