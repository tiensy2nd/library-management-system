﻿using Library_Management_System.Edit_Models;
using Library_Management_System.Models;
using Library_Management_System.Extensions;

namespace Library_Management_System.Services
{
    public class UserServices : IUserServices
    {
        private readonly LMS_PRN211Context _context;

        public UserServices(LMS_PRN211Context context)
        {
            _context = context;
        }

        public void AddToken(string token, int userId)
        {
            Token tokenObj = new Token
            {
                Id = GetLastTokenId() + 1,
                Token1 = token,
                UserId = userId,
                GeneratedTime = DateTime.Now,
                Status = "U"
            };

            _context.Tokens.Add(tokenObj);
            _context.SaveChanges();
        }

        /// <summary>
        /// Add a user to database
        /// </summary>
        /// <param name="user">user to be added</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">when user argument is null</exception>
        /// <exception cref="Exception">when user's id or email address already exists in the database</exception>
        public User? AddUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("Add user to database: user is null");
            }

            User? u = GetUserById(user.Id);
            if (u != null)
            {
                throw new Exception("Cannot add user, userID already exist");
            }

            u = GetUserByEmail(user.Email);
            if (u != null)
            {
                throw new Exception("Cannot add user, userID already exist");
            }

            _context.Users.Add(user);
            return user;
        }

        /// <summary>
        /// Create new user. This function is different from add user is that we only need to 
        /// provide a UserEditModel, contains basic information Email, Password, RetypePassword, 
        /// Role of a User, then this function will perform add user to the database (if meet all 
        /// constrains). RoleList is not relevant in this function, it exists because this EditModel 
        /// is also used in Razor page where I need to load a list of all the roles
        /// </summary>
        /// <param name="userEditModel">user to be added to the database</param>
        /// <returns>user object added to the database</returns>
        /// <exception cref="Exception">Exception are thrown when duplicate email or password does not match</exception>
        public User? CreateUser(UserEditModel userEditModel)
        {
            if (!userEditModel.Password.Equals(userEditModel.RetypePwd))
            {
                throw new Exception("Password does not match");
            }
            // generate salt for new account
            byte[] salt = LoginUtilities.GenerateSalt();
            byte[] hashedPwd = LoginUtilities.HashPassword(userEditModel.Password, salt);

            // generate user object to be added
            User? user = new User
            {
                Id = GetLastId() + 1,
                Email = userEditModel.Email,
                Salt = salt,
                HashedPassword = hashedPwd,
                Role = userEditModel.RoleId,
                Status = "UNACTIVATE"
            };

            try
            {
                _context.Users.Add(user);

                // depending on roleId, add account to appropriate table
                if (userEditModel.RoleId == 1) // librarian
                {
                    Librarian librarian = new Librarian
                    {
                        Id = user.Id
                    };
                    _context.Librarians.Add(librarian);
                }

                if (userEditModel.RoleId == 2) // borrower
                {
                    Borrower borrower = new Borrower
                    {
                        Id = user.Id
                    };
                    _context.Borrowers.Add(borrower);
                }

                // TODO: Do the same for Author

                _context.SaveChanges();
                return user;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<Borrower> GetBorrowers()
        {
            return _context.Borrowers.ToList();
        }

        /// <summary>
        /// Get lastest userId in the User table, or 0 if User table is empty
        /// </summary>
        /// <returns></returns>
        public int GetLastId()
        {
            int lastId = _context.Users.DefaultIfEmpty().Max(u => u == null ? 0 : u.Id);
            return lastId;
        }

        public int GetLastTokenId()
        {
            return _context.Tokens.DefaultIfEmpty().Max(t => t == null ? 0 : t.Id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="librarianId"></param>
        /// <returns></returns>
        public Librarian? GetLibrarianById(int librarianId)
        {
            Librarian? librarian = null;
            librarian = _context.Librarians.SingleOrDefault(l => l.Id == librarianId);
            return librarian;
        }

        /// <summary>
        /// Get all user's roles in the system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserRole> GetRoles()
        {
            IEnumerable<UserRole> roles = _context.UserRoles;
            return roles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public User? GetUserByEmail(string email)
        {
            User? user = _context.Users.SingleOrDefault(u => u.Email.Equals(email));
            return user;
        }

        public User? GetUserById(int id)
        {
            User? user = _context.Users.SingleOrDefault(u => u.Id == id);
            return user;
        }

        public User? GetUserByToken(string token)
        {
            User? user = _context.Tokens.SingleOrDefault(t => t.Token1.Equals(token)).User;
            return user;
        }

        public IEnumerable<User> GetUsers()
        {
            IEnumerable<User> users = _context.Users;
            return users;
        }

        public void UpdateUser(User user)
        {
            _context.Users.Update(user);
            _context.SaveChanges();
        }
    }
}
