﻿using Library_Management_System.Models;

namespace Library_Management_System.ViewModels
{
    public class BookAddModel
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? ISBN { get; set; }
        public int edition { get; set; }

        public string? publisher { get; set; }
        public string? description {  get; set; }
        public string? location { get; set; }
        public int num_of_copies { get; set; }
        public string? format { get; set; }

    }
}
